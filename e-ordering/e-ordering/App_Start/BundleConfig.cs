﻿using System.Web;
using System.Web.Optimization;

namespace e_ordering
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/vendor/dist/jquery-3.5.1.slim.min.js",
                        "~/vendor/dist/bootstrap.bundle.min.js"
                        ));
*/

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js"
                       //"~/vendor/dist/jquery-3.5.1.slim.min.js",
                       //"~/vendor/dist/bootstrap.bundle.min.js"
                       ));

          

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/vendor/css").Include(
                    "~/vendor/css/prism-coy.css",
                    "~/vendor/css/style.css",
                    "~/vendor/css/translateelement.css",
                    "~/vendor/css/custom.css"));


            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                        "~/vendor/js/vendor-all.min.js",
                        "~/vendor/js/bootstrap.min.js",
                        "~/vendor/js/ripple.js",
                        "~/vendor/js/pcoded.min.js",
                        "~/vendor/js/menu-setting.min.js",
                        "~/vendor/js/prism.js",
                        "~/vendor/js/horizontal-menu.js",
                        "~/vendor/js/analytics.js"

                        //"~/vendor/js/apexcharts.min.js",
                        //"~/vendor/js/dashboard.min.js"
                        ));
            
        }
    }
}
