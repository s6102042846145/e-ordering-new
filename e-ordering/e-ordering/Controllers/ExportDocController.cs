﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_ordering.Controllers
{
    public class ExportDocController : Controller
    {
        // GET: ExportDoc
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult EndCustomerEmail()
        {
            return View();
        }
        public ActionResult CustomerEmail()
        {
            return View();
        }
        public ActionResult ODSReportAuthorize()
        {
            return View();
        }
        public ActionResult MailLog()
        {
            return View();
        }
        public ActionResult ScrapReturnMaster()
        {
            return View();
        }
        public ActionResult OpData()
        {
            return View();
        }
        public ActionResult InLandCost()
        {
            return View();
        }
        public ActionResult ScheduleShipment()
        {
            return View();
        }
        public ActionResult Vessl()
        {
            return View();
        }
        public ActionResult VesslAgent()
        {
            return View();
        }
        public ActionResult VesslType()
        {
            return View();
        }
        public ActionResult ShipmentPeriod()
        {
            return View();
        }

        
    }
}