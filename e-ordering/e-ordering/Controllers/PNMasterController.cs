﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_ordering.Controllers
{
    public class PNMasterController : Controller
    {
        // GET: PNMaster
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SectionMaster()
        {
            return View();
        }
        public ActionResult SectionHierarchyMaster() 
        {
            return View();
        }
        public ActionResult SizeMaster()
        {
            return View();
        }
        public ActionResult SizeMasterForm()
        {
            return View();
        }
        public ActionResult StandardMaster()
        {
            return View();
        }
        public ActionResult GrandeMaster()
        {
            return View();
        }
        public ActionResult StandardGrade()
        {
            return View();
        }
        public ActionResult PNStatus()
        {
            return View();
        }
        public ActionResult PNStatusSize()
        {
            return View();
        }
        public ActionResult StorageNewPN()
        {
            return View();
        }
        public ActionResult GroupVCMaster()
        {
            return View();
        }
        public ActionResult SalesORG()
        {
            return View();
        }
        public ActionResult GroupVCCost()
        {
            return View();
        }
    }
}